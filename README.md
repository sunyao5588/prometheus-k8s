# prometheus-k8s

prometheus for kubernetes monitoring
step by step:
1. kubectl create -f blackbox
2. kubectl create -f node-exporter
3. kubectl create -f kube-state-metrics/kube-state/
4. kubectl create -f prometheus
5. kubectl create -f grafana
   
   
    
tips: node-exporter is used for k8s nodes monitoring      
      blackbox is used for k8s network monitoring     
      kube-state-metrics is used to generating metrics from k8s   
      prometheus is a monitoring and alerting tool    
      grafana is a json show page     
